﻿using System;

namespace CarloFirstDungeon
{		
		
		public class CarlosDungeon
		{
				public static string m_heroClass;
				public static string m_playerName;
				public static string m_heroItem;
				public static string m_roomItem;
				public static string m_roomName;
				/// <summary>
				/// The entry point of the program, where the program control starts and ends.
				/// </summary>
				/// <param name="args">The command-line arguments.</param>
				public static void Main (string[] args)
				{	
					m_heroItem = "a Scanning Tool";
					Console.Title = "DISTRESS SIGNAL";
					Console.BackgroundColor = ConsoleColor.Black;
					Console.ForegroundColor = ConsoleColor.Green;
					Console.Clear();

					Console.WriteLine ("WELCOME TO MY FRIST DUNGEON!\n\n");

					Console.WriteLine("Answering a mysterious distress signal a small recon spaceship approaches a space station,\n" +
									  " there don't seem to be any signs of life coming from it.\n" +
									  "The ship's pilot initiates the docking procedure.\n\n");

					Choice_PlayerName ();

					Choice_heroClass ();

					Console.Clear ();
					Console.WriteLine (m_playerName + " the " + m_heroClass + " opens the airlock and steps onto the station.\n\n" +
									   "Press any Key to proceed.");

					Console.ReadKey();

					EntranceRoom.EnterRoom ();

				}

				public static void Choice_PlayerName()
				{
					Console.WriteLine ("DATA ARCHIVE CORRUPTED.\n\n" +
									   "plase enter your Name.\n\n");

				     m_playerName = Console.ReadLine();
				}

				public static void Choice_heroClass ()
				{
					Console.Clear();
					Console.WriteLine (m_playerName + ", in what type of field-work were you trained?\n\n"+
										"1-Soldier: I specialize in close-quarter and ranged combat.\n" +
										"2-Engineer: I can repair and hack into most kinds of computers and security systems.\n" +
										"3-Biologist: I study and collect alien-life specimens.\n\n");

					ConsoleKey pressedKey = Console.ReadKey().Key;

					switch (pressedKey)
					{
						case(ConsoleKey.D1):
						{
							m_heroClass = "Soldier";
							break;
						}

						case(ConsoleKey.D2):
						{
							m_heroClass = "Engineer";
							break;
						}

						case(ConsoleKey.D3):
						{
							m_heroClass = "Biologist";
							break;
						}

						default:
						{
							Console.Clear();
							Console.WriteLine ("ERROR: The archive has no record of this specialization being taught at your training academy.\n" +
											   "please choose a valid specialization.");
							Choice_heroClass ();
							break;
						}

					}
				}

				public static void Print_HeroItem()
				{
					Console.WriteLine("You have " + CarlosDungeon.m_heroItem + " with you.\n\n");
				}

		


		}
}

