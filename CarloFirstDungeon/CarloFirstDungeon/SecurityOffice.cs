﻿using System;

namespace CarloFirstDungeon
{
	public class SecurityOffice
	{	
		public static string m_roomItem = "a Broken Pistol";
		public static void EnterRoom()
		{
			Console.Clear();
			CarlosDungeon.m_roomName = "Security Office";
			Console.WriteLine("This was the command center for the station's security service.\n" +
							  "From here it woud be possible to monitor the whole area, if only the Console was powered up.\n" +
							  "Still, it's probably worth it to took around for anything that might be useful.");
						if(CarlosDungeon.m_heroClass == "Soldier")
						{
							SecOffSoldier();
						}
						else if(CarlosDungeon.m_heroClass == "Engineer")
						{
							SecOffEngineer();
						}
						else
						{
							SecOffBiologist();
						}
			

		}



		static void SecOffSoldier()
		{	
			Console.WriteLine("On the Eastern side of the room you see a door to the VIP Lounge, on the opposite side one to the Lost and found.\n\n" +
							  "1-Search the room.\n" +
							  "2-Go back to the Guest Welcome center.\n" +
							  "3-Go to the VIP Lounge.\n" +
							  "4-Go to the Lost and Found.");
			ConsoleKey pressedkey = Console.ReadKey().Key;

			switch(pressedkey)
			{
				case(ConsoleKey.D1):
				{
					SearchRoom();
					break;
				}
				case(ConsoleKey.D2):
				{
					
					Console.Clear();
					EntranceRoom.EnterRoom();
					break;
				}
				case(ConsoleKey.D3):
				{
					Console.Clear();
					VIPLounge.EnterRoom();
					break;
				}
				case(ConsoleKey.D4):
				{
					Console.Clear();
					LostAndFound.EnterRoom();
					break;
				}
				default:
				{
					Console.Clear();
					Console.WriteLine("PLEASE ENTER A VALID IMPUT");
					Console.ReadKey();
					Console.Clear();
					SecurityOffice.EnterRoom();
					break;
				}

			}
		}

		static void SecOffEngineer()
		{	
			Console.WriteLine("On the Northern side of the room you see a door to the Guest Welcome area, on the Western side one to the Lost and found.\n\n" +
							  "1-Search the room.\n" +
							  "2-Go back to the VIP Lounge.\n" +
							  "3-Go to the Guest Welcome Center\n" +
							  "4-Go to the Lost and Found");
			ConsoleKey pressedkey = Console.ReadKey().Key;

			switch(pressedkey)
			{
				case(ConsoleKey.D1):
				{
					SearchRoom();
					break;
				}
				case(ConsoleKey.D2):
				{
					Console.Clear();
					VIPLounge.EnterRoom();
					break;
				}
				case(ConsoleKey.D3):
				{
					Console.Clear();
					Console.WriteLine("Unfortunately, the door is still jammed, you will have to take the long way around.\n\n" +
									  "Any Key-Go Back.");
					Console.ReadKey();
					SecurityOffice.EnterRoom();
					break;
				}
				case(ConsoleKey.D4):
				{
					Console.Clear();
					LostAndFound.EnterRoom();
					break;
				}
				default:
				{
					Console.Clear();
					Console.WriteLine("PLEASE ENTER A VALID IMPUT");
					Console.ReadKey();
					Console.Clear();
					SecurityOffice.EnterRoom();
					break;
				}

			}
		}

		static void SecOffBiologist()
		{	
			Console.WriteLine("On the Northern side of the room you see a door to the Guest Welcome area, on the Eastern side one to the VIP Lounge.\n\n" +
							  "1-Search the room.\n" +
							  "2-Go back to the Lost and Found.\n" +
							  "3-Go to the Guest Welcome Center\n" +
							  "4-Go to the VIP Lounge");	
			ConsoleKey pressedkey = Console.ReadKey().Key;

			switch(pressedkey)
			{
				case(ConsoleKey.D1):
				{
					SearchRoom();
					break;
				}
				case(ConsoleKey.D2):
				{
					
					Console.Clear();
					LostAndFound.EnterRoom();
					break;
				}
				case(ConsoleKey.D3):
				{
					Console.Clear();
					Console.WriteLine("Unfortunately, the door is still jammed, you will have to take the long way around.\n\n" +
									  "Any Key-Go Back.");
					Console.ReadKey();
					SecurityOffice.EnterRoom();
					break;
				}
				case(ConsoleKey.D4):
				{
					Console.Clear();
					VIPLounge.EnterRoom();
					break;
				}
				default:
				{
					Console.Clear();
					Console.WriteLine("PLEASE ENTER A VALID IMPUT");
					Console.ReadKey();
					Console.Clear();
					SecurityOffice.EnterRoom();
					break;
				}

			}
		}

		public static void SearchRoom()
		{
				Console.Clear();
				Console.WriteLine("You search the " + CarlosDungeon.m_roomName + " for clues and useful objects and find " + m_roomItem + ".\n\n" +
								  "Take " + m_roomItem + "?\n\n" +
								  "1-Take it with you.\n" +
								  "2-Leave it where it is.\n");

			ConsoleKey pressedKey = Console.ReadKey().Key;

			switch(pressedKey)
			{
				case(ConsoleKey.D1):
				{
					string tempItem = CarlosDungeon.m_heroItem;
					CarlosDungeon.m_heroItem = m_roomItem;
					m_roomItem = tempItem;

					Console.WriteLine("You picked up " + CarlosDungeon.m_heroItem + ", but had to leave " + m_roomItem +".\n\n" +
									  "Any Key- Proceed.");
					Console.ReadKey();
					LostAndFound.EnterRoom();
					break;
				}

				case(ConsoleKey.D2):
				{
					Console.WriteLine("You leave " + m_roomItem + " where it is." );
					SecurityOffice.EnterRoom();
					break;
				}
				default:
				{

					Console.Clear();
					Console.WriteLine("PLEASE ENTER A VALID IMPUT");
					Console.ReadKey();
					Console.Clear();
					SearchRoom();
					break;
		
				}
			}
		}
	}
}

