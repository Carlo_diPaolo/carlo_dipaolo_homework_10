﻿using System;

namespace CarloFirstDungeon
{
	public class LostAndFound
	{
		public static string m_roomItem = "an Assault Rifle";
		public static void EnterRoom ()
		{
			Console.Clear();
			CarlosDungeon.m_roomName = "Lost and Found";
			Console.WriteLine("Here is where all the lost items and forgotten luggage was stored, surely there must be something of value somewhere in here.");

			if(CarlosDungeon.m_heroClass == "Biologist")
			{
				Console.WriteLine("On the eastern side of the Room you see a door to the Security office\n\n");
				CarlosDungeon.Print_HeroItem();
				Console.WriteLine("1-Explore room.\n" +
								  "2-Go back to the Guest Welcome Center\n" +
								  "3-Go to the Security Office");
				ConsoleKey pressedkey = Console.ReadKey().Key;

				switch(pressedkey)
				{
						case(ConsoleKey.D1):
						{
						SearchRoom();
						break;
						}
					case(ConsoleKey.D2):
					{
						Console.Clear();
						EntranceRoom.EnterRoom();
						break;
					}
					case(ConsoleKey.D3):
					{
						Console.Clear();
						SecurityOffice.EnterRoom();
						break;
					}
					default:
					{
						Console.Clear();
						Console.WriteLine("PLEASE ENTER A VALID IMPUT");
						Console.ReadKey();
						Console.Clear();
						LostAndFound.EnterRoom();
						break;
					}
				}
			}
			else
			{
				Console.WriteLine("On the Northern side of the Room you see a door to the Guest Welcome Center\n\n" +
								  "1-Explore room.\n" +
								  "2-Go back to the Security Office\n" +
								  "3-Go to the Guest Welcome Center");
				ConsoleKey pressedkey = Console.ReadKey().Key;

				switch(pressedkey)
				{
					case(ConsoleKey.D1):
					{
						SearchRoom();
						break;
					}
					case(ConsoleKey.D2):
					{
						Console.Clear();
						SecurityOffice.EnterRoom();
						break;
					}
					case(ConsoleKey.D3):
					{
						Console.Clear();
						Console.WriteLine("The strange organic growth still blocks the path, you will have to take the long way around.\n\n" +
										  "Any Key-Go Back.");
						Console.ReadKey();
						VIPLounge.EnterRoom();
						break;
					}
					default:
					{
						Console.Clear();
						Console.WriteLine("PLEASE ENTER A VALID IMPUT");
						Console.ReadKey();
						Console.Clear();
						LostAndFound.EnterRoom();
						break;
					}
				}
			}
		}

		public static void SearchRoom()
		{
				Console.Clear();
				Console.WriteLine("You search the " + CarlosDungeon.m_roomName + " for clues and useful objects and find " + m_roomItem + ".\n\n" +
								  "Take " + m_roomItem + "?\n\n" +
								  "1-Take it with you.\n" +
								  "2-Leave it where it is.\n");

			ConsoleKey pressedKey = Console.ReadKey().Key;

			switch(pressedKey)
			{
				case(ConsoleKey.D1):
				{
					string tempItem = CarlosDungeon.m_heroItem;
					CarlosDungeon.m_heroItem = m_roomItem;
					m_roomItem = tempItem;

					Console.WriteLine("You picked up " + CarlosDungeon.m_heroItem + ", but had to leave " + m_roomItem +".\n\n" +
									  "Any Key- Proceed.");
					Console.ReadKey();
					LostAndFound.EnterRoom();
					break;
				}

				case(ConsoleKey.D2):
				{
					Console.WriteLine("You leave " + m_roomItem + " where it is." );
					LostAndFound.EnterRoom();
					break;
				}
				default:
				{

					Console.Clear();
					Console.WriteLine("PLEASE ENTER A VALID IMPUT");
					Console.ReadKey();
					Console.Clear();
					SearchRoom();
					break;
		
				}
			}
		}
	}
}

