﻿using System;

namespace CarloFirstDungeon
{
	public class EntranceRoom
	{	
		public static bool m_blastedDoor = false;
		public static bool m_hackedDoor = false;
		public static bool m_frozenVines = false;

	
		public static void EnterRoom ()
		{
			Console.Clear();
			CarlosDungeon.m_roomName = "Guest Welcome Center";
			Console.WriteLine("You are in the station's former guest welcome area, the airlock has sealed shut, seemingly o it's own.");

			if (CarlosDungeon.m_heroClass == "Engineer")
			{
			 Console.WriteLine("The system seems to be offline and any hacking attempts prove useless.");
			}

			Console.WriteLine("You will have to find another way back to your ship.\n\n");
			CarlosDungeon.Print_HeroItem();
			Console.WriteLine("Which door will you check?\n" +
							  "1-The Security Office.\n" +
							  "2-The VIP Lounge.\n" +
							  "3-The Lost and Found Storage.\n" +
							  "4-The Metro Station");

			ConsoleKey pressedKey= Console.ReadKey().Key;

			switch (pressedKey)
			{
				case(ConsoleKey.D1):
				{
					if(!m_blastedDoor)
					{
						Console.Clear();
						Console.WriteLine("The door is jammed.");

						if(CarlosDungeon.m_heroClass == "Soldier")
						{
							Console.WriteLine("One well-placed explosive charge should be enough to blow it open.\n\n" +
											  "1-Place ad activate Explosive Charge\n" +
											  "2-Go Back\n\n");
							SoldierDoorChoice ();
							
						}

						else
						{
							Console.WriteLine("You try to pry it open, but you are unable to move it even an inch.\n\n" +
											  "Any Key-Go Back.");
							
							Console.ReadKey();
							EntranceRoom.EnterRoom();
						}
					}

					else
					{
						Console.Clear();
						Console.WriteLine("The door is still torn open.\n" +
										  "Any Key-Proceed");
						Console.ReadKey();
						SecurityOffice.EnterRoom();
					}
					break;
				}
				case(ConsoleKey.D2):
				{
					if(!m_hackedDoor)
					{
						Console.Clear();
						Console.WriteLine("The security system has closed the Lounge's blast doors.");

						if(CarlosDungeon.m_heroClass == "Engineer")
						{
							Console.WriteLine("Fortunately the system has also activated the door's emergency generator.\n\n" +
											  "1-Hack Door\n" +
											  "2-Go Back");
							EngineerDoorChoice();
						}
						else if(CarlosDungeon.m_heroClass == "Soldier")
						{
							Console.WriteLine ("That means explosive charges would be useless against it.\n\n" +
											   "Any Key- Go Back.");
							Console.ReadKey();
							EntranceRoom.EnterRoom();
						}
						else
						{
							Console.WriteLine("There doesn't seem to be any way to open it.\n\n" +
											  "Any Key- Go Back.");
							Console.ReadKey();
							EntranceRoom.EnterRoom();
						}
					}

					else
					{
						Console.Clear();
						Console.WriteLine("The Lounge's door is open\n\n" +
										  "Any Key- Proceed");
						Console.ReadKey();
						VIPLounge.EnterRoom();
					}
					break;
				}

				case(ConsoleKey.D3):
				{
					if(!m_frozenVines)
					{
						Console.Clear();
						Console.WriteLine("The door if blocked with some sort of organic growth, like roots made of flesh.\n" +
										  "It is flexible but at the same time very resistant.\n");
						if(CarlosDungeon.m_heroClass == "Biologist")
						{
							Console.WriteLine("You have seen something like this in a laboratory one time and you know the only way to stop it "+
											  "from immediately regenerating is to freeze it.\n" +
											  "Luckily, you have a coule canisters of standard-issue cooling fluid with you.\n\n"+
											  "1-Use Cooling Fluid.\n" +
											  "2-Go Back");
							BiologistDoorChoice();
						}

						else if(CarlosDungeon.m_heroClass == "Soldier")
						{
							Console.WriteLine ("That means explosive charges would be useless against it.\n\n" +
											   "Any Key- Go Back.");
							Console.ReadKey();
							EntranceRoom.EnterRoom();
						}

						else
						{
							Console.WriteLine ("There doesn't seem to be any way to get through at the moment.\n\n" +
											   "Any Key- Go Back.");
							Console.ReadKey();
							EntranceRoom.EnterRoom();
						}
					 }

					 else
					 {
					 	Console.Clear();
						Console.WriteLine("The way is clear, that strange orgnism doesn't seem to be growing back, for now.\n\n" +
										  "Any Key- Proceed");
						Console.ReadKey();
						LostAndFound.EnterRoom();

					 }
					break;
					
				}

				case(ConsoleKey.D4):
				{
					MetroStation.EnterRoom();
					break;
				}


				default:
				{   Console.Clear();
					Console.WriteLine("PLEASE ENTER A VALID IMPUT");
					Console.ReadKey();
					Console.Clear();
					EntranceRoom.EnterRoom();
					break;
				}
			}

		}

		static void SoldierDoorChoice()
		{
			ConsoleKey doorKey = Console.ReadKey().Key;

				switch (doorKey)
				{
					case(ConsoleKey.D1):
					{	
						Console.Clear();
						Console.WriteLine("You activate the charge and take cover from the ensuing blast.\n" +
										  "When the smoke clears the door is torn open.\n\n" +
										  "Any Key-Proceed.");
						m_blastedDoor =true;
						Console.ReadKey();
						SecurityOffice.EnterRoom();
						break;
					}

					case(ConsoleKey.D2):
					{
						Console.Clear();
						EntranceRoom.EnterRoom();
						break;
					}
					default:
					{
						Console.Clear();
						Console.WriteLine("PLEASE ENTER A VALID IMPUT");
						Console.ReadKey();
						Console.Clear();
						SoldierDoorChoice();
						break;
					}
				}
		}

		static void EngineerDoorChoice()
		{
			ConsoleKey doorKey = Console.ReadKey().Key;	

				switch (doorKey)
				{

					case (ConsoleKey.D1):
					{
						Console.Clear();
						Console.WriteLine("You are easily able to break into the door's security system and open it.\n\n" +
										  "Any Key-Proceed.");
						m_hackedDoor = true;
						Console.ReadKey();
						VIPLounge.EnterRoom();
						break;
					}

					case(ConsoleKey.D2):
					{
						Console.Clear();
						EntranceRoom.EnterRoom();
						break;
					}

					default:
					{
						Console.Clear();
						Console.WriteLine("PLEASE ENTER A VALID IMPUT");
						Console.ReadKey();
						Console.Clear();
						EngineerDoorChoice();
						break;
					}
				}
				
		}

		static void BiologistDoorChoice()
		{
			ConsoleKey doorKey = Console.ReadKey().Key;

				switch (doorKey)
				{
					case(ConsoleKey.D1):
					{
						Console.Clear(); 
						Console.WriteLine("You spray the Cooling Fluid on the Growth.\n" +
										  "Then, hittig it breaks it into splinters.\n\n"+
										  "Any Key-Proceed.");
						m_frozenVines = true;
						Console.ReadKey();
						LostAndFound.EnterRoom();
						break;
					}

					case(ConsoleKey.D2):
					{
						Console.Clear();
						EntranceRoom.EnterRoom();
						break;
					}
					default:
					{
						Console.Clear();
						Console.WriteLine("PLEASE ENTER A VALID IMPUT");
						Console.ReadKey();
						Console.Clear();
						BiologistDoorChoice();
						break;
					}
				}
		}
	}
}

