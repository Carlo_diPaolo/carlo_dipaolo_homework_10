﻿using System;

namespace CarloFirstDungeon
{
	public class MetroStation
	{
		public static void EnterRoom ()
		{
			Console.Clear();
			Console.WriteLine("From this area, people coult take the trains that connected the various sectors of the station.\n"+
							  "Maybe some of them are still operational.\n"+
							  "You see an Emergency Repair Station on a wall and on the other side of the room a hallway that leads to the Purple Line.\n");

							  CarlosDungeon.Print_HeroItem(); 

			 Console.WriteLine("1- Use the Repair Station.\n"+
							  "2- Go to the Hallway.\n" +
							  "3-Go back to the Guest welcome Area.\n");

			ConsoleKey pressedKey = Console.ReadKey().Key;

			switch(pressedKey)
			{
				case(ConsoleKey.D1):
				{
					Console.Clear();
					RepairStation();
					break;
				}

				case(ConsoleKey.D2):
				{
					Console.Clear();
					DangerHallway.EnterRoom();
					break;
				}

				case(ConsoleKey.D3):
				{
					Console.Clear();
					EntranceRoom.EnterRoom();
					break;
				}

				default:
				{
					Console.Clear();
					Console.WriteLine("PLEASE ENTER A VALID IMPUT");
					Console.ReadKey();
					Console.Clear();
					MetroStation.EnterRoom();
					break;
				}
			}
		}
			public static void RepairStation()
			{
				if(CarlosDungeon.m_heroClass == "Engineer" && CarlosDungeon.m_heroItem == "a Broken Pistol")
				{
				 Console.WriteLine("Use the tools to fix the Broken Pistol?\n\n" +
				 				   "1-Yes.\n" +
				 				   "2-No.\n");

 				 ConsoleKey pressedKey = Console.ReadKey().Key;

 				 switch(pressedKey)
 				 {
 				 	case(ConsoleKey.D1):
 				 	{
 				 		Console.Clear();
	 				 	CarlosDungeon.m_heroItem = "a Pistol";
	 				 	Console.WriteLine("You now have " + CarlosDungeon.m_heroItem + ".");
	 				 	Console.ReadKey();
	 				 	MetroStation.EnterRoom();
	 				 	break;
	 				}

	 				case(ConsoleKey.D2):
	 				{
	 					MetroStation.EnterRoom();
	 					break;
	 				}

					default:
					{
						Console.Clear();
						Console.WriteLine("PLEASE ENTER A VALID IMPUT");
						Console.ReadKey();
						Console.Clear();
						MetroStation.EnterRoom();
						break;
					}
 				  }
				}

			  else if(CarlosDungeon.m_heroClass == "Biologist" && CarlosDungeon.m_heroItem == "a Case of instable Chemicals")
			  {
			  	Console.WriteLine("Use the tools to combine the chemicals?\n\n" +
			  					  "1-Yes.\n" +
			  					  "2-No.");
				ConsoleKey pressedKey = Console.ReadKey().Key;

 				 switch(pressedKey)
 				 {
 				 	case(ConsoleKey.D1):
 				 	{
 				 		Console.Clear();
	 				 	CarlosDungeon.m_heroItem = "Incendiary Bombs";
	 				 	Console.WriteLine("You now have " + CarlosDungeon.m_heroItem + ".");
	 				 	Console.ReadKey();
	 				 	MetroStation.EnterRoom();
	 				 	break;
	 				}

	 				case(ConsoleKey.D2):
	 				{
	 					MetroStation.EnterRoom();
	 					break;
	 				}

					default:
					{
						Console.Clear();
						Console.WriteLine("PLEASE ENTER A VALID IMPUT");
						Console.ReadKey();
						MetroStation.EnterRoom();
						break;
					}
 				  }
			  }

			  else if(CarlosDungeon.m_heroClass == "Soldier" && CarlosDungeon.m_heroItem == "an Assault Rifle")
			  {
			  	Console.Clear();
			  	Console.WriteLine("You alway found technical stuff way to complicated to learn, plus, your Rifle semms to be perfectly functional.\n");
			  	Console.ReadKey();
			  	MetroStation.EnterRoom();
			  }

			  else
			  {
			  	Console.Clear();
			  	Console.WriteLine("There doesn't seem to be anything you can do here at the moment, perheps try searching for other objects?");
				Console.ReadKey();
			  	MetroStation.EnterRoom();
			  }

		}
	}
}



