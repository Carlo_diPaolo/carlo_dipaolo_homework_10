﻿using System;

namespace CarloFirstDungeon
{
	public class DangerHallway
	{
		public static bool m_monsterEncountered = false;
		public static bool m_monsterKilled = false;
		public static void EnterRoom ()
		{
			Console.Clear();
			if(!m_monsterKilled && !m_monsterEncountered)
			{
				Console.Clear();
				Console.WriteLine("You are walking down the Hallway when you hear a gurgling noise a few meters ahead of you.\n" +
								  "You hide near the wall and see some kind of creature bent over what appears to have been a person.\n" +
								  "It doesn't seem to have noticed you yet, but you will have to go past it to get to the train.\n\n");

				m_monsterEncountered = true;

				CarlosDungeon.Print_HeroItem();

				Console.WriteLine("1-Attack the Creature:.\n\n" +
								  "2-Go back to the Entrance of the Station.");

				ConsoleKey pressedKey = Console.ReadKey().Key;

				switch(pressedKey)
				{
					case(ConsoleKey.D1):
					{
						AttackMonster();
						break;
					}

					case(ConsoleKey.D2):
					{
						MetroStation.EnterRoom();
						break;
					}

					default:
					{
						Console.Clear();
						Console.WriteLine("PLEASE ENTER A VALID IMPUT");
						Console.ReadKey();
						Console.Clear();
						DangerHallway.EnterRoom();
						break;
					}
				}
			}

			else if(!m_monsterKilled && m_monsterEncountered)
			{
				Console.Clear();
				Console.WriteLine("You return to the hallway, but the creature is still there.");

				CarlosDungeon.Print_HeroItem();

				Console.WriteLine("1-Attack the Creature:.\n\n" +
								  "2-Go back to the Entrance of the Station.");

				ConsoleKey pressedKey = Console.ReadKey().Key;

				switch(pressedKey)
				{
					case(ConsoleKey.D1):
					{
						AttackMonster();
						break;
					}

					case(ConsoleKey.D2):
					{
						MetroStation.EnterRoom();
						break;
					}

					default:
					{
						Console.Clear();
						Console.WriteLine("PLEASE ENTER A VALID IMPUT");
						Console.ReadKey();
						Console.Clear();
						DangerHallway.EnterRoom();
						break;
					}
				}
			}

			else
			{
				Console.Clear();
				Console.WriteLine("The hallway seems to be free from danger, for now.");
				Console.ReadKey();
			}
		}

		public static void AttackMonster()
		{
			if(CarlosDungeon.m_heroClass == "Soldier" && CarlosDungeon.m_heroItem == "an Assault Rifle")
			{
				Console.Clear();
				Console.WriteLine("The creature turn towards you and starts charging!\n" +
								  "You aim where it's face would be,if it had one, and fire.\n" +
								  "It falls to the ground and starts sliding, until it is lying at your feet, dead.\n\n" +
								  "Any Key-Proceed.");

				m_monsterKilled = true;
				Console.ReadKey();
				DangerHallway.EnterRoom();
			}

			else if (CarlosDungeon.m_heroClass == "Engineer" && CarlosDungeon.m_heroItem == "a Pistol")
			{
				Console.Clear();
				Console.WriteLine("The creature turn towards you and starts charging!\n" +
								  "You aim where it's face would be,if it had one, and fire.\n" +
								  "But one bullet is not enough, so you unload the whole clip.\n" +
								  "Finally it stops running and slams against a wall, apparently dead.\n\n" +
								  "Any Key-Proceed.");

				m_monsterKilled = true;
				Console.ReadKey();
				DangerHallway.EnterRoom();

			}

			else if (CarlosDungeon.m_heroClass == "Biologist" && CarlosDungeon.m_heroItem == "Incendiary Bombs")
			{
				Console.Clear();
				Console.WriteLine("You throw one of the bombs towards the creature.\n" +
								  "as it is enveloped by the flames it starts screaming and slamming around the hallway.\n" +
								  "After a while it falls to the ground, dead.\n" +
								  "Any Key-Proceed.");

				m_monsterKilled = true;
				Console.ReadKey();
				DangerHallway.EnterRoom();
			}

			else if (CarlosDungeon.m_heroClass != "Soldier" && CarlosDungeon.m_heroItem == "an Assault Rifle")
			{
				Console.Clear();
				Console.WriteLine("The creature turn towards you and starts charging!\n" +
								  "You aim where it's face would be,if it had one, and fire.\n" +
								  "But you were not trained to use heavy weapons and can't aim effectivelly.\n" +
								  "The creture jumps on you and sinks it's teeth in your neck.\n\n" +
								  "You are dead.");

				Console.ReadKey();
				GameOver.Death();
			}

			else if (CarlosDungeon.m_heroItem == "a Broken Pistol")
			{
				Console.Clear();
				Console.WriteLine("The creature turn towards you and starts charging!\n" +
								  "You aim where it's face would be,if it had one, and fire.\n" +
								  "But your Pistol is broken and nothing happens.\n" +
								  "The creture jumps on you and sinks it's teeth in your neck.\n\n" +
								  "You are dead.");

				Console.ReadKey();
				GameOver.Death();
			}

			else
			{
				Console.Clear();
				Console.WriteLine("The creature turn towards you and starts charging!\n" +
								  "You are completely unprepared.\n" +
								  "The creture jumps on you and sinks it's teeth in your neck.\n\n" +
								  "You are dead.");

				Console.ReadKey();
				GameOver.Death();
			}
		}
	}
}

