﻿using System;

namespace CarloFirstDungeon
{
	public class VIPLounge
	{
		
		public static string m_roomItem = "a Case of instable Chemicals";
		public static void EnterRoom()
		{
			Console.Clear();
			CarlosDungeon.m_roomName = "VIP Lounge";
			Console.WriteLine("This is the Area where the frist class passengers of the interplanetary shuttles could realax between flights.\n" +
							  "It seems completely empty now, but it might be worth it to take a look around.");

			

			if(CarlosDungeon.m_heroClass == "Engineer")
			{
				Console.WriteLine("On the Western side of the room you see a door to the Security Office\n\n");

				CarlosDungeon.Print_HeroItem();

				Console.WriteLine("1-Explore room.\n" +
								  "2-Go back to the Guest Welcome Center.\n" +
								  "3-Go to the Security Office.");

				ConsoleKey pressedkey = Console.ReadKey().Key;

				switch(pressedkey)
				{
						case(ConsoleKey.D1):
						{
							SearchRoom();
							break;
						}
						case(ConsoleKey.D2):
						{
							Console.Clear();
							EntranceRoom.EnterRoom();
							break;
						}
						case(ConsoleKey.D3):
						{
							Console.Clear();
							SecurityOffice.EnterRoom();
							break;
						}
						default:
						{
							Console.Clear();
							Console.WriteLine("PLEASE ENTER A VALID IMPUT");
							Console.ReadKey();
							Console.Clear();
							VIPLounge.EnterRoom();
							break;
						}
					}
				}

			else
			{

				Console.WriteLine("On the Northern side of the room you see a door to the Guest Welcome Center\n\n");
				CarlosDungeon.Print_HeroItem();
				Console.WriteLine("1-Explore room.\n" +
								  "2-Go back to the Security Office\n" +
								  "3-Go to the Guest Welcome Center");

				ConsoleKey pressedkey = Console.ReadKey().Key;

				switch(pressedkey)
				{
					case(ConsoleKey.D1):
					{
						SearchRoom();
						break;
					}
					case(ConsoleKey.D2):
					{
						Console.Clear();
						SecurityOffice.EnterRoom();
						break;
					}
					case(ConsoleKey.D3):
					{
						Console.Clear();
						Console.WriteLine("The Blast Doors are still sealed shut, you will have to take the long way around.\n\n" +
										  "Any Key-Go Back.");
						Console.ReadKey();
						VIPLounge.EnterRoom();
						break;
					}
					default:
					{
						Console.Clear();
						Console.WriteLine("PLEASE ENTER A VALID IMPUT");
						Console.ReadKey();
						Console.Clear();
						VIPLounge.EnterRoom();
						break;
					}
				}
			
			}
		}
		public static void SearchRoom()
		{
				Console.Clear();
				Console.WriteLine("You search the " + CarlosDungeon.m_roomName + " for clues and useful objects and find  " + m_roomItem + ".\n\n" +
								  "Take " + m_roomItem + "?\n\n" +
								  "1-Take it with you.\n" +
								  "2-Leave it where it is.\n");

			ConsoleKey pressedKey = Console.ReadKey().Key;

			switch(pressedKey)
			{
				case(ConsoleKey.D1):
				{
					string tempItem = CarlosDungeon.m_heroItem;
					CarlosDungeon.m_heroItem = m_roomItem;
					m_roomItem = tempItem;

					Console.WriteLine("You picked up " + CarlosDungeon.m_heroItem + ", but had to leave " + m_roomItem +".\n\n" +
									  "Any Key- Proceed.");
					Console.ReadKey();
					VIPLounge.EnterRoom();
					break;
				}

				case(ConsoleKey.D2):
				{
					Console.WriteLine("You leave " + m_roomItem + " where it is." );
					VIPLounge.EnterRoom();
					break;
				}
				default:
				{

					Console.Clear();
					Console.WriteLine("PLEASE ENTER A VALID IMPUT");
					Console.ReadKey();
					Console.Clear();
					SearchRoom();
					break;
		
				}
			}
		}
	}
}

